import {Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Banner from '../components/Banner';


export default function ErrorPage(){

	const data = {
		title: "404 - Page not found",
		content: "The page you are looking cannot be found.",
		destination: "/",
		label: "Back"

	}


	return(
		<Banner bannerProp= {data}/>
		// <div id="error">
			
		// 	<h1>Error 404</h1>
		// 	<p>It looks like nothing was found at this location.
        //        Maybe try one of the links in the menu or press back to go to the Home page.
        //     </p>
        //     <Button variant='primary' as={Link} to="">Back</Button>*/}
        // </div>
		)
}