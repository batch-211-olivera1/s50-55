// import coursesData from '../data/coursesData';
import {useState, useEffect, useContext} from 'react';
import CourseCard from '../components/CourseCard';


export default function Courses(){

	// console.log(coursesData)

	//using the map array method, we can loop through our courseData and dynamically render any number of CourseCards depending on how many array elements are present in our data

	//map returns an array, which we can display in the page via the component function's return statement

	//props are a way to pass any valid JS data from parent component to child component.

	//You can pass as many props as you want. Even functions can be passed as props.


	const [ courses, setCourses ] = useState([])
	useEffect(()=>{
		fetch('http://localhost:4000/courses/all')
		.then(res=>res.json())
		.then(data=>{
		const coursesArr = (data.map((course) => {
		
		return (
			<CourseCard courseProp={course} key={course._id}/>
			)

	}))
		setCourses(coursesArr)
	})
	},[courses])
	

	// const courses = coursesData.map((course) => {
	// 	// console.log(course)
	// 	return <CourseCard courseProp={course} key={course.id}/>
	// })

	return(
		<>
		
			{courses}
		</>
	)
}

/*Array of component is called a list*/