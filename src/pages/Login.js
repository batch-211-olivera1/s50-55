import {Form, Button} from 'react-bootstrap';
//Complete (3) Hooks of React
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {


  const [email, setEmail] = useState ('');
  const [password, setPassword] = useState ('');
  const [isActive, setIsActive] = useState ('');

  console.log(email);
  console.log(password);

  // Allows us to consume the User Context/Data and its properties for validation
  const { user, setUser } = useContext(UserContext);


  function authenticate(e) {

      e.preventDefault()


      fetch('http://localhost:4000/users/login', {
        method: 'POST',
        headers:{
          'Content-Type':'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data);
        if(typeof data.access !== "undefined"){
          localStorage.setItem('token',data.access)
          retrieveUserDetails(data.access)
          Swal.fire({
            title: "Login Successful!",
            icon: "success",
            text: "Welcome to booking app of 211"
          })
        }else{
          Swal.fire({
            title: "Authentication Failed!",
            icon: "error",
            text: "Check your credentials"
          })
        }
      })


      const retrieveUserDetails = (token) =>{

        fetch('http://localhost:4000/users/details', {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        .then(res=>res.json())
        .then(data=>{
          console.log(data);

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        })
      }

      //set the email of the authenticated user in the local storage
      /*
        Syntax:
            localStorage.setItem("propertyName",value)
      */

      // localStorage.setItem("email",email)
      //the "localStorage.setItem" allows us to manipulate the browser's localStorage property to store information indefinitely to help demonstrate conditional rendering and the login and logout features.
      //Because React JS is a single page application, using the localStorage does not trigger rerendering of components and for us to be able to view the effects of this we would need to refresh our browser.
      //The proper solution to this will be discussed in the next session.

      // Set the global user state to have properties obtained from local storage.
      // This will pass the data to the UserContext which is ready to use from all different endpoints/pages
      // setUser({
      //   email: localStorage.getItem('email')
      // })

      setEmail("");
      setPassword("");

      // alert(`${email} has been verified! Welcome back!`)
  }

  useEffect(()=>{

    if(email !== "" && password !== ""){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
 

  }, [email, password])



  return (
    // Conditional Rendering
    // LOGIC - if there is a user logged-in in the web application, enpoint or "/login" should not be accesible. The user should be navigated to courses tab instead.

    (user.id !== null)
    ?
    <Navigate to="/courses"/>
    // ELSE - if the localStorage is empty, the user is allowed to access the login page.
    :
    <Form onSubmit={(e)=>authenticate(e)}>
      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
        type="email"
        placeholder="Enter your email"
        value={email}
        onChange={e=>setEmail(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Password"
        value={password}
        onChange={e=>setPassword(e.target.value)}
        required
        />
      </Form.Group>
      
      { isActive ?
      <Button variant="success" type="submit" id="submitBtn">
        Login
      </Button>
      :
      <Button variant="danger" type="submit" id="submitBtn" disabled>
        Login
      </Button>
      }

    </Form>
  );
}
