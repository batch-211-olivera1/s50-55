// import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}){
	// console.log(props)

	// userState hook:
	/*
		-A hook in react is a kind of tool. The useState hook allows creation and manipulation of states
		-States are a way for React to keep track of any value and associate it with a component
		-When a state changes, React re-renders ONLY the specific components or part of the component that changed (and not the entire page or components whose states have not changed)
	*/
	
	// State: a special kind of variable (can be named anything) that React uses to render/re-render componenets when needed
	// State setter: are the only way to change a state's value. By convention they are named after the state
	// default state: The state's initial value on "page load"
	
	// -Array destructuring to get the state and the setter 
	

	let { name, description, price, _id } = courseProp;
	
	// Syntax: const [state, setState = useState(default state)]
	// const [count, setCount] = useState(0)
	// const [seat, setSeat] = useState(10)
	
	// function enroll(){
	// if (avail === 0){
		
	// 	// console.log('Enrollees: ' + count)
	// 		alert('Available enrollees reached')
	// 	}else{
	// 		setCount(count + 1);
	// 		setAvail(avail - 1)
	// 	}
	// }

	// function enroll(){
	// if (count !== 10){
	// 		setCount(count + 1);
	// 		setSeat(seat - 1);		
	// 	}
	// }

//useEffect
	// Syntax: useEffect(function,[dependencies])
	
	// useEffect(()=>{
	// 	if(seat===0){
	// 		alert("No more seats available")
	// 	}
	// },[count,seat])

	return(
		<Card className="p-3 mb-3">
			<Card.Body className="card-purple">
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>
				{/*<Card.Text>Enrollees: {count}</Card.Text>
				<Card.Text>Seats: {seat}</Card.Text>*/}
				<Button as={Link} to={`/courses/${_id}`}>Details</Button>
			</Card.Body>
		</Card>
	)
}
